package ir.patmat.note.model.input;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LoginUserInputDTO {
    @NotNull(message = "Email address can't be empty!")
    private String emailAddress;

    @NotNull(message = "Password can't be empty!")
    private String password;
}
