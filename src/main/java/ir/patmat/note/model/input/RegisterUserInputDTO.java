package ir.patmat.note.model.input;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RegisterUserInputDTO {
    @NotNull(message = "Full name can't be empty!")
    private String fullName;

    @Schema(name = "emailAddress", example = "emailaddress@email.com")
    @Pattern(regexp = "^[\\w-.]+@([\\w-]+\\.)+[\\w-]{2,4}$", message = "Email address is NOT valid!")
    @NotNull(message = "Email address can't be empty!")
    private String emailAddress;

    @NotNull(message = "Password can't be empty!")
    private String password;
}
