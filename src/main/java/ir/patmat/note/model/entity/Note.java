package ir.patmat.note.model.entity;

import ir.patmat.note.model.entity.relationship.UserCategory;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.ColumnDefault;

import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "note")
public class Note {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String title;

    private String text;

    @ColumnDefault("CURRENT_TIMESTAMP")
    @org.hibernate.annotations.Generated
    private Date date;

    @ManyToOne
    @JoinColumn(name = "user_category_id")
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private UserCategory userCategory;
}
