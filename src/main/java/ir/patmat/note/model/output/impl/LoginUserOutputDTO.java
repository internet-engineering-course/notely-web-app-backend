package ir.patmat.note.model.output.impl;

import com.fasterxml.jackson.annotation.JsonProperty;
import ir.patmat.note.model.output.OutputDTO;
import lombok.*;

@EqualsAndHashCode(callSuper = true)
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LoginUserOutputDTO extends OutputDTO {
    @JsonProperty("access_token")
    private String accessToken;
}
