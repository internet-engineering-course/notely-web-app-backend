package ir.patmat.note.model.output.impl;

import com.fasterxml.jackson.annotation.JsonProperty;
import ir.patmat.note.model.output.OutputDTO;
import lombok.*;

@EqualsAndHashCode(callSuper = true)
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RegisterUserOutputDTO extends OutputDTO {
    @JsonProperty("message")
    private String message;
}
