package ir.patmat.note.exception_controller;

import ir.patmat.note.exception.UserNotFoundException;
import ir.patmat.note.exception.UsernameAlreadyExistsException;
import ir.patmat.note.model.output.Response;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class UserExceptionController {
    @ExceptionHandler(value = UserNotFoundException.class)
    public ResponseEntity<Response> userNotFoundException() {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(Response.builder().message("User not found!").build());
    }

    @ExceptionHandler(value = UsernameAlreadyExistsException.class)
    public ResponseEntity<Response> usernameAlreadyExistsException() {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(Response.builder().message("Username already exists!").build());
    }

    @ExceptionHandler(value = BadCredentialsException.class)
    public ResponseEntity<Response> badCredentialsException() {
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(Response.builder().message("Not valid credentials!").build());
    }
}
