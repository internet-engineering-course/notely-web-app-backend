package ir.patmat.note.exception_controller;

import ir.patmat.note.model.output.Response;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class DTOValidationExceptionController {

    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public ResponseEntity<Response> handleValidationExceptions(MethodArgumentNotValidException ex) {
        StringBuilder errorMessages = new StringBuilder();
        ex.getBindingResult().getAllErrors().forEach(error -> errorMessages.append(error.getDefaultMessage()).append("\n"));

        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(Response.builder().message(String.valueOf(errorMessages)).build());
    }
}
