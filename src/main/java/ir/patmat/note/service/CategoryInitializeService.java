package ir.patmat.note.service;

import ir.patmat.note.config.CategoryConfig;
import ir.patmat.note.dao.CategoryRepo;
import ir.patmat.note.model.entity.Category;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class CategoryInitializeService {
    private final CategoryConfig categoryConfig;
    private final CategoryRepo categoryRepo;

    @PostConstruct
    private void initializeCategoryTable() {
        categoryConfig.getNames().stream()
                .filter(catName -> !categoryRepo.existsByName(catName))
                .forEach(catName -> categoryRepo.save(Category.builder().name(catName).build()));
    }
}
