package ir.patmat.note.service;

import ir.patmat.note.dao.UserRepo;
import ir.patmat.note.exception.UserNotFoundException;
import ir.patmat.note.exception.UsernameAlreadyExistsException;
import ir.patmat.note.model.entity.User;
import ir.patmat.note.model.input.LoginUserInputDTO;
import ir.patmat.note.model.input.RegisterUserInputDTO;
import ir.patmat.note.model.output.Response;
import ir.patmat.note.model.output.impl.LoginUserOutputDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
@RequiredArgsConstructor
@Slf4j
public class AuthService {
    private final UserRepo userRepo;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final AuthenticationManager authenticationManager;

    public Response register(RegisterUserInputDTO registerUserInputDTO) throws UsernameAlreadyExistsException {
        if (userRepo.existsByUsername(registerUserInputDTO.getEmailAddress()))
            throw new UsernameAlreadyExistsException();

        registerUserInputDTO.setPassword(passwordEncoder.encode(registerUserInputDTO.getPassword()));
        userRepo.save(User.builder().fullName(registerUserInputDTO.getFullName())
                .username(registerUserInputDTO.getEmailAddress())
                .password(registerUserInputDTO.getPassword()).build());

        log.info(String.format("User with username \"%s\" is registered successfully!",
                registerUserInputDTO.getEmailAddress()));

        return Response.builder().message("Successful registration!").build();
    }

    public Response login(LoginUserInputDTO loginUserInputDTO) throws UserNotFoundException {
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginUserInputDTO.getEmailAddress(),
                loginUserInputDTO.getPassword()));

        User user = userRepo.findByUsername(loginUserInputDTO.getEmailAddress()).orElseThrow(UserNotFoundException::new);

        var jwtToken = jwtService.generateToken(Map.of("fullName", user.getFullName()), user);
        return Response.builder().message("Successful login!")
                .data(LoginUserOutputDTO.builder().accessToken(jwtToken).build())
                .build();
    }
}
