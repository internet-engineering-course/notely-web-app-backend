package ir.patmat.note.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
@ConfigurationProperties(prefix = "category")
@Getter
@Setter
public class CategoryConfig {
    private List<String> names;
}
