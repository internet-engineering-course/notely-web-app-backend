package ir.patmat.note.controller;

import ir.patmat.note.exception.UserNotFoundException;
import ir.patmat.note.exception.UsernameAlreadyExistsException;
import ir.patmat.note.model.input.LoginUserInputDTO;
import ir.patmat.note.model.input.RegisterUserInputDTO;
import ir.patmat.note.model.output.Response;
import ir.patmat.note.service.AuthService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@CrossOrigin
public class AuthController {
    final AuthService service;

    @PostMapping("/register")
    public ResponseEntity<Response> register(@Valid @RequestBody RegisterUserInputDTO registerUserInputDTO) throws UsernameAlreadyExistsException {
        return ResponseEntity.ok(service.register(registerUserInputDTO));
    }

    @PostMapping("/login")
    public ResponseEntity<Response> login(@Valid @RequestBody LoginUserInputDTO loginUserInputDTO) throws UserNotFoundException {
        return ResponseEntity.ok(service.login(loginUserInputDTO));
    }
}
