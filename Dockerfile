# FROM maven:3.6.3-openjdk-17 As builder

# ADD ./pom.xml pom.xml
# ADD .mvn .mvn
# ADD mvnw .
# ADD mvnw.cmd .
# ADD ./src src/

# RUN mvn clean
# RUN mvn compile
# RUN mvn package -DskipTests=true

# FROM openjdk:17.0-jdk
# COPY --from=builder target/*.jar app.jar
# EXPOSE 8080
# ENTRYPOINT ["java","-jar","/app.jar"]


FROM maven:3.6.3-openjdk-17
WORKDIR /app

ADD ./pom.xml pom.xml
ADD .mvn .mvn
ADD mvnw .
ADD mvnw.cmd .
ADD ./src src/

# COPY . .

RUN mvn clean
RUN mvn compile
RUN mvn package -DskipTests=true

CMD mvn spring-boot:run
